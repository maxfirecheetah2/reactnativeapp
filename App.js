import React from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity } from 'react-native';

export default class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      username: 'Username',
      password: 'Password',
      loginResultText: null
    };
  }

  render() {
    const onPress = () => this.setState({loginResultText: 'Success!'});
    return (
      <View style={styles.container}>
        <View style={{backgroundColor: 'white', width: 350, height: 250, alignItems: 'center', justifyContent: 'center', marginBottom: 15}}>
          <Text style={{fontSize: 22, fontWeight: 'bold', marginBottom: 15, color: 'gray'}}>Login</Text>
          <TextInput
            underlineColorAndroid="#487496"
            style={{height: 40, width: 300, borderColor: 'gray', borderWidth: 1, marginBottom: 15}}
            onChangeText={(username) => this.setState({username})}
            value={this.state.username}
          />
          <TextInput
            secureTextEntry={true}
            underlineColorAndroid="#487496"
            style={{height: 40, width: 300, borderColor: 'gray', borderWidth: 1, marginBottom: 15}}
            onChangeText={(password) => this.setState({password})}
            value={this.state.password}
          />
          <TouchableOpacity style={styles.button} onPress={onPress}>
            <Text> Login </Text>
          </TouchableOpacity>
        </View>
        {this.state.loginResultText ? <View style={styles.statusBox}>
          <Text style={styles.loginStatusText}>{this.state.loginResultText}</Text>
        </View> : null}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#487496',
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
    width: 300
  },
  statusBox: {
    backgroundColor: '#20cb31',
    width: 350,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center'
  },
  loginStatusText: {
    fontSize: 18,
    fontWeight: 'bold',
    color: 'white'
  }
});
